<?php

set_include_path("google-api-php-client-master/src/" . PATH_SEPARATOR . get_include_path());

require_once "Google/Client.php";
require_once "Google/Service/Analytics.php";

session_start();

$client = new Google_Client();
$client->setApplicationName ('Hello Analytics API Sample');

$client->setClientId('360380193142-spj1vhui5snesph6ponh0290pbia3lre.apps.googleusercontent.com');
$client->setClientSecret('eZfimCux3-a9P0ikO1hg9Dy-');
$client->setRedirectUri('http://localhost/google-analytics');
$client->setDeveloperKey('AIzaSyAz3azdkzTXBzCr8ViffHPjYaVBo1RHjpc');
$client->setScopes(array('https://www.googleapis.com/auth/analytics.readonly'));

// $client->setUseObjects(true);

if (isset($_GET['code'])) 
{
  $client->authenticate();
  $_SESSION['token'] = $client->getAccessToken();
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  header('Location: ' . filter_var ($redirect, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) 
{
  $client->setAccessToken($_SESSION['token']);
}

if (!$client->getAccessToken()) 
{
  $authUrl = $client->createAuthUrl();
  print "<a class='login' href='$authUrl'>Connect Me!</a>";

} 
else 
{
  $analytics = new Google_AnalyticsService($client);
  runMainDemo($analytics);
}

function runMainDemo (&$analytics) 
{
  try 
  {

    // Step 2. Get the user's first view (profile) ID.
    $profileId = getFirstProfileId($analytics);

    if (isset($profileId)) {

      // Step 3. Query the Core Reporting API.
      $results = getResults($analytics, $profileId);

      // Step 4. Output the results.
      printResults($results);
    }

  } 
  catch (apiServiceException $e) 
  {
    // Error from the API.
    print 'There was an API error : ' . $e->getCode() . ' : ' . $e->getMessage();

  } 
  catch (Exception $e) 
  {
    print 'There wan a general error : ' . $e->getMessage();
  }
}

function getFirstprofileId (&$analytics) 
{
  $accounts = $analytics->management_accounts->listManagementAccounts();

  if (count ($accounts->getItems()) > 0) 
  {
    $items = $accounts->getItems();
    $firstAccountId = $items[0]->getId();

    $webproperties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);

    if (count($webproperties->getItems()) > 0) 
    {
      $items = $webproperties->getItems();
      $firstWebpropertyId = $items[0]->getId();

      $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstWebpropertyId);

      if (count($profiles->getItems()) > 0) 
      {
        $items = $profiles->getItems();
        return $items[0]->getId();

      } 
      else 
      {
        throw new Exception('No views (profiles) found for this user.');
      }
    } 
    else 
    {
      throw new Exception('No webproperties found for this user.');
    }
  } 
  else 
  {
    throw new Exception('No accounts found for this user.');
  }
}

function getResults (&$analytics, $profileId) 
{
  return $analytics->data_ga->get
  (
    'ga:' . $profileId,
    '2014-02-22',
    '2014-02-22',
    'ga:sessions'
  );
}

function printResults (&$results) 
{
  if (count ($results->getRows ()) > 0) 
  {
    $profileName = $results->getProfileInfo()->getProfileName();
    $rows = $results->getRows();
    $sessions = $rows[0][0];

    print "<p>First view (profile) found: $profileName</p>";
    print "<p>Total sessions: $sessions</p>";

  } 
  else 
  {
    print '<p>No results found.</p>';
  }
}
