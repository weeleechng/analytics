<code>This script is no longer in use</code>
<?php
die ();
/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
include_once "google-api-php-client-master/examples/templates/base.php";
// echo pageHeader("Simple API Access");

/************************************************
  Make a simple API request using a key. In this
  example we're not making a request as a
  specific user, but simply indicating that the
  request comes from our application, and hence
  should use our quota, which is higher than the
  anonymous quota (which is limited per IP).
 ************************************************/
set_include_path("google-api-php-client-master/src/" . PATH_SEPARATOR . get_include_path());
require_once 'Google/Client.php';
require_once 'Google/Service/Analytics.php';

/************************************************
  We create the client and set the simple API
  access key. If you comment out the call to
  setDeveloperKey, the request may still succeed
  using the anonymous quota.
 ************************************************/

$client_id = '38414000887-j9oivd4h230iorcko70fvvvp6iv5h00m.apps.googleusercontent.com'; //Client ID
$service_account_name = '38414000887-j9oivd4h230iorcko70fvvvp6iv5h00m@developer.gserviceaccount.com'; //Email Address 
$key_file_location = 'estorm_car-0018aceeff70.p12'; //key.p12

$client = new Google_Client();
$client->setApplicationName("Estorm_Analytics_Report_Downloader");

$service = new Google_Service_Analytics($client);

if (isset ($_SESSION['service_token'])) 
{
  $client->setAccessToken ($_SESSION['service_token']);
}

$key = file_get_contents($key_file_location);
$cred = new Google_Auth_AssertionCredentials ($service_account_name, array('https://www.googleapis.com/auth/analytics'), $key);
$client->setAssertionCredentials($cred);
if($client->getAuth()->isAccessTokenExpired()) 
{
  $client->getAuth()->refreshTokenWithAssertion ($cred);
}

$_SESSION['service_token'] = $client->getAccessToken();

$allTraffic = array
(
  'dimensions' => 'ga:source,ga:medium,ga:landingPagePath',
  'metrics' => 'ga:bounceRate,ga:pageviewsPerSession,ga:avgSessionDuration,ga:goal1Completions,ga:goal1ConversionRate',
);

$optParams = array
(
  'dimensions' => $allTraffic['dimensions'],
  'metrics' => $allTraffic['metrics'],
  'max-results' => 9999
);

$results = $service->data_ga->get
(
  'ga:86170911',
  '2014-08-01',
  '2014-08-31',
  'ga:userType',
  $optParams
);

var_dump ($results->rows);

// echo pageFooter(__FILE__);
