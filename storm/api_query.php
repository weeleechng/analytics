<?php

$QUERY_ONLY = false;
$MAX_ROWS = 10000;
$KEY_PATH = 'keys/';
$REPORT_CONFIG_PATH = 'reports/';
$reportfilepattern = '/^.+(\.json)$/U';

$mssql_db = null;

include_once "include/functions.php";
include_once "include/db_credentials.php";
include_once "include/db_functions.php";

$reportdate = getdate_start_end ();
$reportfiles = scandir ($REPORT_CONFIG_PATH);

?><!DOCTYPE html>
<head>
<title>Google Analytics Report Transfer - estorm International</title>
<style>
body { background: #345; color: #ccc; font-size: 14px; }
</style>
</head>
<body><?php

$process_timer_start = new DateTime ();
$totalrecords = 0;

if (isset ($_GET['storm_mode']) && $_GET['storm_mode'] == 'test')
{
  $QUERY_ONLY = true;
}

if (count ($reportfiles) > 0)
{
  if ($QUERY_ONLY == false) 
  {
    $mssql_db = mssql_connect_init ($dsn, $username, $password);
  }
  if ($mssql_db == false && $QUERY_ONLY == false)
  {
    printline (get_time_elapsed ($process_timer_start) . ' - dB connection unavailable');
  }
  else
  {
    printline ('Current datetime: ' . date ('Y-m-d H:i:s'));
    printline ('Reporting date: ' . $reportdate['start'] . ' to ' . $reportdate['end']);
    foreach ($reportfiles as $reportfile)
    {
      if (preg_match ($reportfilepattern, $reportfile))
      {
        printline (get_time_elapsed ($process_timer_start) . ' - Input file: ' . $reportfile);
        $report_file_data = json_decode (file_get_contents ($REPORT_CONFIG_PATH . $reportfile), true);
        $reports = $report_file_data['reports'];
        $accounts = $report_file_data['accounts'];

        foreach ($accounts as $account_name => $account)
        {
          printline (' ================= ');
          printline ('ACCOUNT: ' . $account_name);
          printline ('');

          foreach ($account as $property_id => $account_properties)
          {
            printline ('- Property: ' . $property_id . ' - ' . $account_properties['ga_property_name']);

            foreach ($account_properties['views'] as $view_id => $view_properties)
            {
              printline ('-- View: ' . $view_id . ' - ' . $view_properties['ga_view']);

              if (is_report_within_daterange ($view_properties, $reportdate))
              {
                printline (get_time_elapsed ($process_timer_start) . ' - Report date within range.');

                foreach ($view_properties['reports'] as $report_id)
                {
                  printline ('--- Report: ' . $reports[$report_id]['report_name']);

                  $columns = get_column_array ($reports[$report_id]);
                  
                  $more_columns = ['gaAccount', 'gaProperty', 'gaPropertyName', 'gaViewProfile', 'gaId', 'reportName', 'date'];

                  $check_columns = ['gaProperty', 'gaId', 'reportName', 'date'];
                  $check_values = [ 
                    $property_id,  
                    $view_id, 
                    $reports[$report_id]['report_name'], 
                    $reportdate['end']
                  ];

                  if ($QUERY_ONLY == false)
                  {
                    printline (get_time_elapsed ($process_timer_start) . ' - Checking for existing records in database...');
                    $existscheck = mssql_query_num_records ($mssql_db, $table, $check_columns, $check_values);
                  }

                  if (isset ($existscheck) && $existscheck === false && $QUERY_ONLY == false)
                  {
                    printline (get_time_elapsed ($process_timer_start) . ' - WHOOPSIE! &mdash; There was a problem checking for existing records.');
                  }
                  else if (isset ($existscheck) && $existscheck !== 0 && $QUERY_ONLY == false)
                  {
                    printline (get_time_elapsed ($process_timer_start) . ' - HALT :: report data for [' . $reportdate['end'] . '] already exists in database.');
                    printline ('');
                  }
                  else
                  {                   
                    $result_array = array ();
                    $ga_start_index = 1;
                    printline (get_time_elapsed ($process_timer_start) . ' - No existing records found.');
                    do
                    {
                      printline (get_time_elapsed ($process_timer_start) . ' - Querying Google Analytics, offset: ' . $ga_start_index);
                      $result = execute_ga_query (
                        $report_file_data['email_address'], 
                        $report_file_data['key_file'], 
                        $view_id, 
                        $reports[$report_id]['dimensions'], 
                        $reports[$report_id]['metrics'], 
                        $reportdate,
                        $ga_start_index
                      );

                      if (is_array ($result->rows)) 
                      {
                        $result_array = array_merge ($result_array, $result->rows);
                      }

                      if (count ($result->rows) == $MAX_ROWS)
                      {
                        $ga_start_index += $MAX_ROWS;
                      }
                    }
                    while (count ($result->rows) == $MAX_ROWS);

                    if (count ($result_array) > 0)
                    {
                      $totalrecords += count ($result_array);

                      printline (get_time_elapsed ($process_timer_start) . ' - Retrieved ' . count ($result_array) . ' records. Attempting insert...');
                      // append additional columns to report result
                      foreach ($more_columns as $columnkey => $more_column)
                      {
                        $columns[] = $more_column;
                      }

                      if ($QUERY_ONLY == false)
                      {
                        $columntypes = mssql_column_type ($mssql_db, $table, $columns);
                      }

                      $row_values = array ();

                      // append additional values to report result
                      foreach ($result_array as $row)
                      {
                        $row[] = $account_name;
                        $row[] = $property_id;
                        $row[] = $account_properties['ga_property_name'];
                        $row[] = $view_properties['ga_view'];
                        $row[] = $view_id;
                        $row[] = $reports[$report_id]['report_name'];
                        $row[] = $reportdate['end'];

                        if ($QUERY_ONLY == false) 
                        {
                          $row = value_to_array ($row);
                        }

                        $row_values[] = '(' . implode (',', $row) . ')';
                      }

                      if ($QUERY_ONLY == false)
                      {
                        // insert records into database
                        $count = mssql_row_insert_staggered ($mssql_db, $table, $columns, $row_values);
                        printline (get_time_elapsed ($process_timer_start) . ' - ' . $count . ' rows inserted');
                      }
                      
                    }
                    else
                    {
                      printline (get_time_elapsed ($process_timer_start) . ' - No results from Google Analytics.');
                    } // end count ($result_array)
                    printline ('');

                  } // end if existscheck
                } // end foreach view_properties[reports]
              } 
              else
              {
                printline (get_time_elapsed ($process_timer_start) . ' - Report date not in range.');
                printline ('');
              } // end is_report_within_daterange
            } // end foreach account_properties [views]
          } // end foreach account as account_properties
        } // end foreach accounts as account
      } // end preg_match ($reportfilepattern, $reportfile)
    } // end foreach ($reportfiles as $reportfile)

  }

  mssql_connect_close ($mssql_db);

}
else
{
  printline ('');
  printline ('No report config files found');
}

printline ($totalrecords . ' total records retrieved.');
printline (get_time_elapsed ($process_timer_start) . ' total time elapsed.');

?></body></html>
