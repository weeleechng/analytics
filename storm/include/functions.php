<?php

include_once "../google-api-php-client-master/examples/templates/base.php";
set_include_path ("../google-api-php-client-master/src/" . PATH_SEPARATOR . get_include_path ());
require_once 'Google/Client.php';
require_once 'Google/Service/Analytics.php';

function get_time_elapsed ($startpoint)
{
  $now = new DateTime ();
  $interval = $startpoint->diff ($now);
  $seconds = (intval ($interval->format ('%s')) < 10) ? '0' . $interval->format ('%s') : $interval->format ('%s');
  return $interval->format ('%i:' . $seconds);
}

function printline ($stringline, $raquo = true)
{
  if ($stringline == '')
  {
    print '<br />';
  }
  else
  {
    print '<code>';
    if ($raquo == true) 
    {
      print '&raquo; ';
    }
    print $stringline;
    print '</code><br />';
  }
}

function getdate_start_end ($day = null, $month = null, $year = null, $yesterday = true)
{
  if ($day == null)
  {
    $day = (isset ($_GET['day']) && intval ($_GET['day'] != 0) ? intval ($_GET['day']) : intval (date ('d')) );
  }

  if ($month == null)
  {
    $month = (isset ($_GET['month']) && intval ($_GET['month']) != 0) ? intval ($_GET['month']) : intval (date ('m'));
  }

  if ($year == null)
  {
    $year = (isset ($_GET['year']) && intval ($_GET['year']) != 0) ? intval ($_GET['year']) : intval (date ('Y'));
  } 

  /*
  if (isset ($_GET['month']) && !isset ($_GET['lastmonth']) ||
    isset ($_GET['lastmonth']) && in_array (strtolower ($_GET['lastmonth']), ['0', 'false', 'no', 'n', 'off']))
  {
    $lastmonth = false;
  }

  if ($lastmonth == true)
  {
    $month -= 1;
  }

  if ($month == 0)
  {
    $month = 12;
    $year -= 1;
  }

  */

  if (isset ($_GET['day']) && !isset ($_GET['yesterday']) ||
    isset ($_GET['yesterday']) && in_array (strtolower ($_GET['yesterday']), ['0', 'false', 'no', 'n', 'off']))
  {
    $yesterday = false;
  }

  if ($yesterday == true)
  {
    $day -= 1;
  }

  if ($day == 0)
  {
    $month -= 1;
    if ($month == 0)
    {
      $month = 12;
      $year -= 1;
    }
    $reportday = date ('Y-m-t', mktime (0, 0, 0, $month, 1, $year));
  }
  else
  {
    $reportday = date ('Y-m-d', mktime (0, 0, 0, $month, $day, $year));
  }

  if (isset ($_GET['month']) && isset ($_GET['year']) && !isset ($_GET['day']))
  {
    $returnset = array 
    (
      'start' => date ('Y-m-d', mktime (0, 0, 0, $month, 1, $year)),
      'end' => date ('Y-m-t', mktime (0, 0, 0, $month, 1, $year)),
    );
  }
  else
  {
    $returnset = array 
    (
     'start' => $reportday,
     'end' => $reportday,
    );
  }

  return $returnset;

}

function is_report_within_daterange ($reportconfig, $reportdate)
{
  $retval = false;

  if (isset ($reportconfig['report_start']) && 
    $reportconfig['report_start'] != "" && 
    $reportdate['start'] >= $reportconfig['report_start'])
  {
    $retval = true;
  }

  if (isset ($reportconfig['report_end']) && 
    $reportconfig['report_end'] != "" && 
    $reportdate['end'] > $reportconfig['report_end'])
  {
    $retval = false;
  }

  return $retval;
}

function execute_ga_query ($email, $keyfile, $view_id, $dimensions, $metrics, $reportdate, $start_index = 1)
{
  global $KEY_PATH;

  $email_address = $email;
  $key_file_location = $KEY_PATH . $keyfile; //key.p12

  $client = new Google_Client ();
  $client->setApplicationName ("Estorm_Analytics_Report_Downloader");

  $key = file_get_contents ($key_file_location);
  $cred = new Google_Auth_AssertionCredentials ($email_address, array ('https://www.googleapis.com/auth/analytics.readonly'), $key);
  $client->setAssertionCredentials ($cred);

  if($client->getAuth ()->isAccessTokenExpired ()) 
  {
    $client->getAuth ()->refreshTokenWithAssertion ($cred);
  }

  $_SESSION['service_token'] = $client->getAccessToken ();

  if (isset ($_SESSION['service_token'])) 
  {
    $client->setAccessToken ($_SESSION['service_token']);
  }

  $optParams = array
  (
    'dimensions' => $dimensions,
    'metrics' => $metrics,
    'max-results' => 10000,
    'start-index' => $start_index,
  );

  $service = new Google_Service_Analytics ($client);

  $results = $service->data_ga->get
  (
    'ga:' . str_replace ('ga:', '', $view_id),
    $reportdate['start'],
    $reportdate['end'],
    'ga:userType',
    $optParams
  );

  return $results;
}

function get_column_array ($reportconfig)
{
  $array_column = array ();
  $dimensions = explode (',', $reportconfig['dimensions']);
  $metrics = explode (',', $reportconfig['metrics']);

  foreach ($dimensions as $dimension)
  {
    $array_column[] =  '"' . str_replace ('ga:', '', $dimension) . '"';
  }

  foreach ($metrics as $metric)
  {
    $array_column[] = '"' . str_replace ('ga:', '', $metric) . '"';
  }

  return $array_column;
}
