<?php

/* Default DSN: */
$connect_to = "Azure";

$dsn = "";
$username = "";
$password = "";
$table = "";

$INSERT_LIMIT = 50;

$columntypes = [];

$textcolumntypes = array (
  'text',
  'char',
  'varchar',
  'nvarchar',
  'datetime',
);

function set_db_connect ($connect_to)
{
  global $dsn, $username, $password, $table, $db_credentials;

  $dsn = $db_credentials[$connect_to]["dsn"];
  $username = $db_credentials[$connect_to]["username"];
  $password =  $db_credentials[$connect_to]["password"];
  $table = $db_credentials[$connect_to]["table"];
}

/* Get field types */
function mssql_column_type ($mssql_db, $table, $columns)
{
  $query = 'SELECT TOP 1 ' . implode (',', $columns) . ' FROM ' . $table;
  $columntypes = array ();
  $result = odbc_exec ($mssql_db, $query);
  for ($i = 1; $i <= odbc_num_fields ($result); $i++)
  {
    $columntypes[] = odbc_field_type ($result, $i);
  }

  return $columntypes;
}

function value_to_array ($values)
{
  global $columntypes, $textcolumntypes;
  $index = 0;

  foreach ($values as $key => $value)
  {
    $value = str_replace (['%','$', "'", '\\'], '', $value);

    /* numerify strings */
    if (!in_array ($columntypes[$index], $textcolumntypes))
    {
      $value = intval ($value);
    }
    else
    {
      $value = "'" . $value . "'";
    }

    $values[$key] = $value;
    $index++;
  }

  return $values;
}

/* Initiate database connection */
function mssql_connect_init ($dsn, $username, $password)
{
  try
  {
    // $mssql_db = new PDO ("odbc:$dsn", $username, $password);
    $mssql_db = odbc_connect ($dsn, $username, $password);
    // $mssql_db->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

    return $mssql_db;
  }
  catch (Exception $error)
  { 
    print ("There was an ERROR!<br />" . $error->getMessage());
    return false;
  }

}

/* get number of records that match the criteria */
function mssql_query_num_records ($mssql_db, $table, $columns, $values)
{
  if (count ($columns) != count ($values))
  {
    return false;
  }

  $query = 'SELECT COUNT("rowId") FROM ' . $table . ' WHERE ';
  // $query = 'SELECT TOP (1) rowId FROM ' . $table . ' WHERE ';
  
  $appendstr = '';
  for ($col_i = 0; $col_i < count ($columns); $col_i++)
  {
    if ($appendstr != '')
    {
      $appendstr .= ' AND ';
    }

    $columns[$col_i] = '"' . $columns[$col_i] . '"';
    $values[$col_i] = "'" . $values[$col_i] . "'";
    $appendstr .= $columns[$col_i] . '=' . $values[$col_i];
  }
  
  $query .= $appendstr;

  $execute = odbc_exec ($mssql_db, $query);
  $count = odbc_result ($execute, 1);

  return intval ($count);
  
}

function mssql_row_insert_staggered ($mssql_db, $table, $columns, $values, $do_actual_insert = true)
{
  global $INSERT_LIMIT;
  $success = 0;
  $splitvalues = array ();

  for ($values_index = 0; $values_index < count ($values); $values_index++)
  {
    $splitvalues[] = $values[$values_index];
    if (count ($splitvalues) == $INSERT_LIMIT)
    {
      $insert = mssql_row_insert ($mssql_db, $table, $columns, $splitvalues, $do_actual_insert);
      if ($insert !== false)
      {
        $success += $insert;
      }
      $splitvalues = array ();
    }
  }

  if (count ($splitvalues) != 0)
  {
    $insert = mssql_row_insert ($mssql_db, $table, $columns, $splitvalues, $do_actual_insert);
    if ($insert !== false)
    {
      $success += $insert;
    }
    $splitvalues = array ();
  }

  return $success;
}

/* Insert record into database */
function mssql_row_insert ($mssql_db, $table, $columns, $values, $do_actual_insert = true)
{
  $insert_sql = 'INSERT INTO ' . $table . ' (' . implode (',', $columns) . ') VALUES ' . implode (',', $values);

  if ($do_actual_insert === true)
  {
    try
    {
      $insert_result = odbc_exec ($mssql_db, $insert_sql);
      if ($insert_result === false)
      {
        print '<code>' . $insert_sql . '</code>';
        return false;
      }
      return odbc_num_rows ($insert_result);
    }
    catch (Exception $error)
    {
      print_r ($error->getMessage());
      return false;
    }
  }
  else
  {
    return count ($values);
  }
}

/* Close database connection */
function mssql_connect_close ($mssql_db)
{
  $mssql_db = null;
}


/* Set DSN on page load: */
if (isset ($_GET['connect_to']) && $_GET['connect_to'] != '')
{
  $connect_to = $_GET['connect_to'];
}

set_db_connect ($connect_to);
